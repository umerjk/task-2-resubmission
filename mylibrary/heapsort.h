#ifndef HEAPSORT
#define HEAPSORT
#include<algorithm>

#endif // HEAPSORT

namespace mylib
{
    template <typename iterator>
    void heap_sort(iterator first, iterator last)
    {
        std::make_heap(first, last);
        std::sort_heap(first, last);
    }
}
