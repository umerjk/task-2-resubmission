#ifndef INTROSORT
#define INTROSORT

#include<iterator>
#include<algorithm>
#include<functional>
#include<cmath>
#include<vector>
#include"shellsort.h"
#include"selection_sort.h"
#include"heapsort.h"



namespace mylib
{


    //Insertion Sort...............

    template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>

    void insertion_sort( RandomAcessIterator first, RandomAcessIterator last, Compare cmp = Compare())//, Compare cmp = Compare())
    {
        using size_type = typename RandomAcessIterator::difference_type;
        size_type n {last-first};
        for(size_type i {0}; i<n-1; i++)
        {
            size_type j {i+1};
            size_type k {i};
            for(;j>=1;j--)
            {
                if(cmp(first[j],first[k]))
                {
                    std::swap(first[k],first[j]);
                }
                --k;
            }
        }
    }



    template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>

    void introsort_loop( RandomAcessIterator first, RandomAcessIterator last, typename RandomAcessIterator::difference_type limit_or_floor)//, Compare cmp = Compare())
    {

        //size_type n = last-first;
        //limit_or_floor = 2 * std::floor(std::log2(n));
        int size_threshold = 10;
        while (last-first>size_threshold)
        {
            if(limit_or_floor == 0)
            {
                //selection_sort(first,last);
                insertion_sort(first,last);
                //heap_sort(first,last);
                return;
            }
            else
            {
                --limit_or_floor;
                RandomAcessIterator cut = (first + (last-first)/2);
                //size_type part = std::partition(first, last);
                introsort_loop( cut, last, limit_or_floor);
                last = cut;
            }
        }
    }

    template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>



    void introsort(RandomAcessIterator first, RandomAcessIterator last)//, Compare cmp = Compare())
    {
        using size_type = typename RandomAcessIterator::difference_type;
        size_type depth_limit_or_floor = 2 * std::floor(std::log2(last-first));

        introsort_loop(first,last,depth_limit_or_floor);//,cmp);
        selection_sort(first,last);
        //insertion_sort(first,last);
        //shellSort(first,last);
    }
}


#endif // INTROSORT

